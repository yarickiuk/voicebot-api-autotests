import helper.RMQHelper;
import helper.SqlScriptLeadHelper;
import helper.WaitingHelper;
import model.LeadStatus;
import model.LeadsJsonData;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;

public class ManiManiStatusModel {

    private Integer expectedManiManiPartnerId = 2;
    private String expectedLoanType = "installment";

    LeadStatus expectedLeadStatusAfterLeadCreation = new LeadStatus()
            .setLeadStatus(20)
            .setPartnerAssigningStatus(30)
            .setPartnerProcessingStatus(20)
            .setPartnerCheckStatus(20)
            .setVoiceVerificationStatus(10)
            .setPostbackStatus(null);

    LeadStatus expectedLeadStatusAfterVerificationConfirmResult = new LeadStatus()
            .setLeadStatus(40)
            .setPartnerAssigningStatus(40)
            .setPartnerProcessingStatus(40)
            .setPartnerCheckStatus(20)
            .setVoiceVerificationStatus(20)
            .setPostbackStatus(20);

    @Test
    public void checkStatusesThenVerificationResultConfirm() throws IOException, SQLException, InterruptedException, TimeoutException {
        Integer leadId = this.createNewLead(LeadsJsonData.jsonLeadCorrectDataForRMQ);

        WaitingHelper.waitingVoiceVerificationStatus(leadId, 10);
        assertEquals(expectedManiManiPartnerId, SqlScriptLeadHelper.findPartnerId(leadId));
        assertEquals(expectedLoanType, SqlScriptLeadHelper.findLoanTypeByLeadId(leadId));
        assertEquals(expectedLeadStatusAfterLeadCreation, SqlScriptLeadHelper.queryStatusesByLeadId(leadId));


        RMQHelper.publishVerificationResultMessage(SqlScriptLeadHelper.findLeadUuidByLeadId(leadId), "confirm");
        WaitingHelper.waitingLeadStatus(leadId, 40);
        assertEquals(expectedLeadStatusAfterVerificationConfirmResult, SqlScriptLeadHelper.queryStatusesByLeadId(leadId));
    }

    private Integer createNewLead (String message) throws SQLException, IOException, TimeoutException {
        Integer lastLeadId = SqlScriptLeadHelper.findLastLeadId();
        System.out.println("id нового лида: " + (lastLeadId + 1));
        RMQHelper.publishLeadToIncomingMessage(message);
        return lastLeadId + 1 ;
    }
}
