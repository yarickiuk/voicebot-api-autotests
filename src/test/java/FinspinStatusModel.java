import helper.RMQHelper;
import helper.SqlScriptLeadHelper;
import helper.WaitingHelper;
import io.restassured.http.ContentType;
import model.EndPoints;
import model.LeadStatus;
import model.LeadsJsonData;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

import static io.restassured.RestAssured.given;
import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;


public class FinspinStatusModel {

    private Integer expectedFinspinPartnerId = 1;
    private String expectedLoanType = "PDL";

    LeadStatus expectedLeadStatusAfterLeadCreation = new LeadStatus()
            .setLeadStatus(20)
            .setPartnerAssigningStatus(30)
            .setPartnerProcessingStatus(20)
            .setPartnerCheckStatus(20)
            .setVoiceVerificationStatus(10)
            .setPostbackStatus(null);

    LeadStatus expectedLeadStatusAfterVerificationConfirmResult = new LeadStatus()
            .setLeadStatus(40)
            .setPartnerAssigningStatus(40)
            .setPartnerProcessingStatus(40)
            .setPartnerCheckStatus(20)
            .setVoiceVerificationStatus(20)
            .setPostbackStatus(20);

    LeadStatus expectedLeadStatusAfterVerificationDenyResult = new LeadStatus()
            .setLeadStatus(30)
            .setPartnerAssigningStatus(40)
            .setPartnerProcessingStatus(60)
            .setPartnerCheckStatus(20)
            .setVoiceVerificationStatus(30)
            .setPostbackStatus(null);

    LeadStatus expectedLeadStatusAfterVerificationUnknownResult = new LeadStatus()
            .setLeadStatus(30)
            .setPartnerAssigningStatus(40)
            .setPartnerProcessingStatus(60)
            .setPartnerCheckStatus(20)
            .setVoiceVerificationStatus(40)
            .setPostbackStatus(null);

    @Test
    public void checkLeadStatusAnswerOK() throws SQLException, IOException, TimeoutException, InterruptedException {
        Integer leadId = this.createNewLead();
        WaitingHelper.waitingVoiceVerificationStatus(leadId, 10);

        assertEquals(expectedFinspinPartnerId, SqlScriptLeadHelper.findPartnerId(leadId));
        assertEquals(expectedLoanType, SqlScriptLeadHelper.findLoanTypeByLeadId(leadId));
        assertEquals(expectedLeadStatusAfterLeadCreation, SqlScriptLeadHelper.queryStatusesByLeadId(leadId));

        RMQHelper.publishVerificationResultMessage(SqlScriptLeadHelper.findLeadUuidByLeadId(leadId), "confirm");
        WaitingHelper.waitingLeadStatus(leadId, 40);

        assertEquals(expectedLeadStatusAfterVerificationConfirmResult, SqlScriptLeadHelper.queryStatusesByLeadId(leadId));
    }

    @Test
    public void checkLeadStatusAnswerDeny() throws SQLException, IOException, TimeoutException, InterruptedException {

        Integer leadId = this.createNewLead();
        WaitingHelper.waitingVoiceVerificationStatus(leadId, 10);

        assertEquals(expectedFinspinPartnerId, SqlScriptLeadHelper.findPartnerId(leadId));
        assertEquals(expectedLoanType, SqlScriptLeadHelper.findLoanTypeByLeadId(leadId));
        assertEquals(expectedLeadStatusAfterLeadCreation, SqlScriptLeadHelper.queryStatusesByLeadId(leadId));

        RMQHelper.publishVerificationResultMessage(SqlScriptLeadHelper.findLeadUuidByLeadId(leadId), "deny");
        WaitingHelper.waitingLeadStatus(leadId, 30);
        assertEquals(expectedLeadStatusAfterVerificationDenyResult, SqlScriptLeadHelper.queryStatusesByLeadId(leadId));
    }

    @Test
    public void checkLeadStatusAnswerUnknownResult() throws SQLException, IOException, TimeoutException, InterruptedException {

        Integer leadId = this.createNewLead();
        WaitingHelper.waitingVoiceVerificationStatus(leadId, 10);

        assertEquals(expectedFinspinPartnerId, SqlScriptLeadHelper.findPartnerId(leadId));
        assertEquals(expectedLoanType, SqlScriptLeadHelper.findLoanTypeByLeadId(leadId));
        assertEquals(expectedLeadStatusAfterLeadCreation, SqlScriptLeadHelper.queryStatusesByLeadId(leadId));

        RMQHelper.publishVerificationResultMessage(SqlScriptLeadHelper.findLeadUuidByLeadId(leadId), "unknownResult");
        WaitingHelper.waitingLeadStatus(leadId, 30);
        assertEquals(expectedLeadStatusAfterVerificationUnknownResult, SqlScriptLeadHelper.queryStatusesByLeadId(leadId));
    }

    private Integer createNewLead() throws SQLException {
        given().log().all()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonCorrectData)
                .when()
                .post(EndPoints.createLeads)
                .then().log().all()
                .statusCode(200)
                .assertThat().body(equalTo("[]"));

        Integer leadId = SqlScriptLeadHelper.findLastLeadId();
        System.out.println("последний лид: " + leadId);
        return leadId;
    }
}
