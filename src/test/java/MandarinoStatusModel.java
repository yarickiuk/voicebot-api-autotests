import helper.RMQHelper;
import helper.SqlScriptLeadHelper;
import helper.WaitingHelper;
import io.restassured.http.ContentType;
import model.EndPoints;
import model.LeadStatus;
import model.LeadsJsonData;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;

public class MandarinoStatusModel {
    private Integer expectedMandarinoPartnerId = 3;
    private String expectedLoanType = "PDL";

    LeadStatus expectedLeadStatusAfterLeadCreation = new LeadStatus()
            .setLeadStatus(20)
            .setPartnerAssigningStatus(30)
            .setPartnerProcessingStatus(20)
            .setPartnerCheckStatus(20)
            .setVoiceVerificationStatus(10)
            .setPostbackStatus(null);

    LeadStatus expectedLeadStatusAfterVerificationConfirmResult = new LeadStatus()
            .setLeadStatus(40)
            .setPartnerAssigningStatus(40)
            .setPartnerProcessingStatus(40)
            .setPartnerCheckStatus(20)
            .setVoiceVerificationStatus(20)
            .setPostbackStatus(20);

    @Test
    public void checkLeadStatusAnswerOK() throws SQLException, IOException, TimeoutException, InterruptedException {
        Integer leadId = this.createNewLead();
        WaitingHelper.waitingVoiceVerificationStatus(leadId, 10);

        assertEquals(expectedMandarinoPartnerId, SqlScriptLeadHelper.findPartnerId(leadId));
        assertEquals(expectedLoanType, SqlScriptLeadHelper.findLoanTypeByLeadId(leadId));
        assertEquals(expectedLeadStatusAfterLeadCreation, SqlScriptLeadHelper.queryStatusesByLeadId(leadId));

        RMQHelper.publishVerificationResultMessage(SqlScriptLeadHelper.findLeadUuidByLeadId(leadId), "confirm");
        WaitingHelper.waitingLeadStatus(leadId, 40);

        assertEquals(expectedLeadStatusAfterVerificationConfirmResult, SqlScriptLeadHelper.queryStatusesByLeadId(leadId));
    }
    private Integer createNewLead() throws SQLException {
        given().log().all()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonCorrectDataForMandarinoPartner)
                .when()
                .post(EndPoints.createLeads)
                .then().log().all()
                .statusCode(200)
                .assertThat().body(equalTo("[]"));

        Integer leadId = SqlScriptLeadHelper.findLastLeadId();
        System.out.println("последний лид: " + leadId);
        return leadId;
    }
}
