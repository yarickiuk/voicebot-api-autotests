import io.restassured.http.ContentType;
import model.*;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class CreationLeads {

    @Test
    public void getSuccessStatusForCorrectData() {
        given().log().all()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonCorrectData)
                .when()
                .post(EndPoints.createLeads)
                .then().log().all()
                .statusCode(200)
                .assertThat().body(equalTo("[]"));
    }

    @Test
    public void getFailedStatusForIncorrectDataWithoutContractId() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonWithoutContractId)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"Contract Id cannot be blank.\",\"fieldPath\":\"lead.contractId\"}"));
    }

    @Test
    public void getFailedStatusForIncorrectDataWithoutLeadId() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonWithoutLeadId)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"Lead Id cannot be blank.\",\"fieldPath\":\"lead.leadId\"}"));
    }


    @Test
    public void getFailedStatusForIncorrectDataWithoutTypeId() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonWithoutTypeId)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"Type Id cannot be blank.\",\"fieldPath\":\"product.typeId\"}"));
    }

    @Test
    public void getFailedStatusForIncorrectDataWithoutLastName() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonWithoutLastName)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"Last Name cannot be blank.\",\"fieldPath\":\"client.lastName\"}"));
    }

    @Test
    public void getFailedStatusForIncorrectDataWithoutFirstName() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonWithoutFirstName)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"First Name cannot be blank.\",\"fieldPath\":\"client.firstName\"}"));
    }

    @Test
    public void getFailedStatusForIncorrectDataWithoutAge() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonWithoutAge)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"Age cannot be blank.\",\"fieldPath\":\"client.age\"}"));
    }

    @Test
    public void getFailedStatusForIncorrectDataWithoutPhone() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonWithoutPhone)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"Phone cannot be blank.\",\"fieldPath\":\"client.phone\"}"));
    }

    @Test
    public void getFailedStatusForIncorrectDataWithoutAmount() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonWithoutAmount)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"Amount cannot be blank.\",\"fieldPath\":\"product.amount\"}"));
    }

    @Test
    public void getFailedStatusForIncorrectDataWithoutRegion() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonWithoutRegion)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"Region cannot be blank.\",\"fieldPath\":\"location.region\"}"));
    }

    @Test
    public void getFailedStatusForGetRequest() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonCorrectData)
                .when()
                .get(EndPoints.createLeads)
                .then()
                .statusCode(405)
                .assertThat().body("name", equalTo("Method Not Allowed"))
                .assertThat().body("message", equalTo("Method Not Allowed. This URL can only handle the following request methods: POST."))
                .assertThat().body("status", equalTo(405));
    }

    @Test
    public void getFailedStatusForIncorrectJson() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body("test")
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400);
    }

    @Test
    public void getFailedStatusForIncorrectJsonWrongType() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonIncorrectDataType)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"Amount must be an integer.\",\"fieldPath\":\"product.amount\"}"));
    }

    @Test
    public void getFailedStatusForIncorrectFormatPhoneData() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonIncorrectFormatPhoneData)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"Phone is invalid.\",\"fieldPath\":\"client.phone\"}"));
    }

    @Test
    public void getFailedStatusForIncorrectFormatDateData() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonIncorrectFormatDateData)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"The format of Birth Date is invalid.\",\"fieldPath\":\"client.birthDate\"}"));
    }

    @Test
    public void getFailedStatusForIncorrectFormatEmailData() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(LeadsJsonData.jsonIncorrectFormatEmailData)
                .when()
                .post(EndPoints.createLeads)
                .then()
                .statusCode(400)
                .assertThat().body("details", equalTo("{\"message\":\"Email is not a valid email address.\",\"fieldPath\":\"client.email\"}"));
    }

}