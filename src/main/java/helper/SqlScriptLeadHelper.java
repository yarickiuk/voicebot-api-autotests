package helper;
import model.LeadStatus;
import util.DBClient;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlScriptLeadHelper {



    public static Integer findLastLeadId() throws SQLException {
        Integer idLead;
        ResultSet info = DBClient.getInstance().getConnection().createStatement().executeQuery("" +
                "SELECT id " +
                "FROM lead " +
                "ORDER BY id DESC");
        if (info.next()) {
            idLead = info.getInt("id");
            return idLead;
        }
        return null;
    }

    public static Integer findPartnerId(Integer idLead) throws SQLException{
        Integer partnerId;
        ResultSet info = DBClient.getInstance().getConnection().createStatement().executeQuery("" +
                "SELECT partner_id " +
                "FROM partner_processing " +
                "WHERE partner_processing.lead_id = " + idLead);
        if (info.next()) {
            partnerId = info.getInt("partner_id");
            return partnerId;
        }
        return null;
    }

    public static String findLeadUuidByLeadId(Integer idLead) throws SQLException {
        String uuid;
        ResultSet info = DBClient.getInstance().getConnection().createStatement().executeQuery("" +
                "SELECT uuid " +
                "FROM voice_verification " +
                "WHERE voice_verification.lead_id = " + idLead);
        if (info.next()) {
            uuid = info.getString("uuid");
            return uuid;
        }
        return null;
    }

    public static String findLoanTypeByLeadId(Integer idLead) throws SQLException {
        String loan_type;
        ResultSet info = DBClient.getInstance().getConnection().createStatement().executeQuery("" +
                "SELECT loan_type " +
                "FROM lead " +
                "WHERE lead.id = " + idLead);
        if (info.next()) {
            loan_type = info.getString("loan_type");
            return loan_type;
        }
        return null;
    }

    public static Integer getLeadStatusByLeadId(Integer idLead) throws SQLException {
        Integer lead_status;
        ResultSet info = DBClient.getInstance().getConnection().createStatement().executeQuery("" +
                "SELECT status " +
                "FROM lead " +
                "WHERE lead.id = " + idLead);
        if (info.next()) {
            lead_status = info.getInt("status");
            return lead_status;
        }
        return null;
    }

    public static Integer getPartnerAssigningStatusByLeadId(Integer idLead) throws SQLException {
        Integer partner_assigning_status;
        ResultSet info = DBClient.getInstance().getConnection().createStatement().executeQuery("" +
                "SELECT status " +
                "FROM partner_assigning " +
                "WHERE partner_assigning.lead_id = " + idLead);
        if (info.next()) {
            partner_assigning_status = info.getInt("status");
            return partner_assigning_status;
        }
        return null;
    }

    public static Integer getPartnerProcessingStatusByLeadId(Integer idLead) throws SQLException {
        Integer partner_processing_status;
        ResultSet info = DBClient.getInstance().getConnection().createStatement().executeQuery("" +
                "SELECT status " +
                "FROM partner_processing " +
                "WHERE partner_processing.lead_id = " + idLead);
        if (info.next()) {
            partner_processing_status = info.getInt("status");
            return partner_processing_status;
        }
        return null;
    }

    public static Integer getPartnerCheckStatusByLeadId(Integer idLead) throws SQLException {
        Integer partner_check_status;
        ResultSet info = DBClient.getInstance().getConnection().createStatement().executeQuery("" +
                "SELECT status " +
                "FROM partner_check " +
                "WHERE partner_check.lead_id = " + idLead);
        if (info.next()) {
            partner_check_status = info.getInt("status");
            return partner_check_status;
        }
        return null;
    }

    public static Integer getVoiceVerificationStatusByLeadId(Integer idLead) throws SQLException {
        Integer voice_verification_status;
        ResultSet info = DBClient.getInstance().getConnection().createStatement().executeQuery("" +
                "SELECT status " +
                "FROM voice_verification " +
                "WHERE voice_verification.lead_id = " + idLead);
        if (info.next()) {
            voice_verification_status = info.getInt("status");
            return voice_verification_status;
        }
        return null;
    }

    public static Integer getPostbackStatusByLeadId(Integer idLead) throws SQLException {
        Integer postback_status;
        ResultSet info = DBClient.getInstance().getConnection().createStatement().executeQuery("" +
                "SELECT status " +
                "FROM postback " +
                "WHERE postback.lead_id = " + idLead);
        if (info.next()) {
            postback_status = info.getInt("status");
            return postback_status;
        }
        return null;
    }

    public static LeadStatus queryStatusesByLeadId(Integer idLead) throws SQLException {
       LeadStatus statuses = new LeadStatus();
        ResultSet info = DBClient.getInstance().getConnection().createStatement().executeQuery("" +
                "SELECT lead.status as lead_status, " +
                "partner_assigning.status as partner_assigning_status, " +
                "partner_processing.status as partner_processing_status, " +
                "partner_check.status as partner_check_status, " +
                "voice_verification.status as voice_verification_status, " +
                "postback.status as postback_status " +
                "FROM lead " +
                "LEFT OUTER JOIN partner_assigning " +
                "ON lead.id = partner_assigning.lead_id " +
                "LEFT OUTER JOIN partner_processing " +
                "ON lead.id = partner_processing.lead_id " +
                "LEFT OUTER JOIN partner_check " +
                "ON lead.id = partner_check.lead_id " +
                "LEFT OUTER JOIN voice_verification " +
                "ON lead.id = voice_verification.lead_id " +
                "LEFT OUTER JOIN postback " +
                "ON lead.id = postback.lead_id " +
                "WHERE lead.id = " + idLead);
        if (info.next()) {
            statuses.setLeadStatus((Integer) info.getObject("lead_status"));
            statuses.setPartnerAssigningStatus((Integer) info.getObject("partner_assigning_status"));
            statuses.setPartnerProcessingStatus((Integer) info.getObject("partner_processing_status"));
            statuses.setPartnerCheckStatus((Integer) info.getObject("partner_check_status"));
            statuses.setVoiceVerificationStatus((Integer) info.getObject("voice_verification_status"));
            statuses.setPostbackStatus((Integer) info.getObject("postback_status"));
            return statuses;
        }
        return null;
    }
}
