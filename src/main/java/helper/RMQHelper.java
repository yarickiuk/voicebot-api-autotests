package helper;

import util.RMQFactoryUtil;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RMQHelper {

    public static void publishVerificationResultMessage(String uuid, String result) throws IOException, TimeoutException {
        RMQFactoryUtil rmqFactoryUtil = new RMQFactoryUtil("exbico_test_1");
        String QUEUE_NAME = "verification-result-voice-bot-q-dev";
        String message = "{\"phone\": \"+79193968809\",\"status\": \"" + result + "\",\"messageVersion\": 1,\"objectUuid\": \""+ uuid +"\"}";
        rmqFactoryUtil.publicMessage(QUEUE_NAME, message);
        rmqFactoryUtil.closeConnectionAndChannel();
    }

    public static void publishLeadToIncomingMessage(String message) throws IOException, TimeoutException {
        RMQFactoryUtil rmqFactoryUtil = new RMQFactoryUtil("voicebot_test_1");
        String queue = "voicebot_lead_incoming_q-dev";
        rmqFactoryUtil.publicMessage(queue, message);
        rmqFactoryUtil.closeConnectionAndChannel();
    }
}
