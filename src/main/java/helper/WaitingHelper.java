package helper;

import java.sql.SQLException;
import java.util.function.BooleanSupplier;

public class WaitingHelper {

    public static void waitingLeadStatus (Integer leadId, Integer expectedStatus) throws InterruptedException {
        waitingStatus(() -> {
            try {
                Integer actualStatus = SqlScriptLeadHelper.getLeadStatusByLeadId(leadId);
                return actualStatus != null && actualStatus.equals(expectedStatus);
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    public static void waitingVoiceVerificationStatus (Integer leadId, Integer expectedStatus) throws InterruptedException {
        waitingStatus(() -> {
            try {
                Integer actualStatus = SqlScriptLeadHelper.getVoiceVerificationStatusByLeadId(leadId);
                return actualStatus != null && actualStatus.equals(expectedStatus);
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    private static void waitingStatus (BooleanSupplier supplier) throws InterruptedException {
        for (int i = 0; i < 50; i++) {
            if (supplier.getAsBoolean()) {
                break;
            }
            Thread.sleep(2000);
        }
    }
}
