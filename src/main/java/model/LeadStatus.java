package model;

import java.util.Objects;

public class LeadStatus implements Cloneable {

    private Integer leadStatus;
    private Integer partnerAssigningStatus;
    private Integer partnerProcessingStatus;
    private Integer partnerCheckStatus;
    private Integer voiceVerificationStatus;
    private Integer postbackStatus;

    public LeadStatus() {

    }

    public Integer getLeadStatus() {
        return leadStatus;
    }

    public Integer getPartnerAssigningStatus() {
        return partnerAssigningStatus;
    }

    public Integer getPartnerProcessingStatus() {
        return partnerProcessingStatus;
    }

    public Integer getPartnerCheckStatus() {
        return partnerCheckStatus;
    }

    public Integer getVoiceVerificationStatus() {
        return voiceVerificationStatus;
    }

    public Integer getPostbackStatus() {
        return postbackStatus;
    }

    public LeadStatus setLeadStatus(Integer leadStatus) {
        this.leadStatus = leadStatus;
        return this;
    }

    public LeadStatus setPartnerAssigningStatus(Integer partnerAssigningStatus) {
        this.partnerAssigningStatus = partnerAssigningStatus;
        return this;
    }

    public LeadStatus setPartnerProcessingStatus(Integer partnerProcessingStatus) {
        this.partnerProcessingStatus = partnerProcessingStatus;
        return this;
    }

    public LeadStatus setPartnerCheckStatus(Integer partnerCheckStatus) {
        this.partnerCheckStatus = partnerCheckStatus;
        return this;
    }

    public LeadStatus setVoiceVerificationStatus(Integer voiceVerificationStatus) {
        this.voiceVerificationStatus = voiceVerificationStatus;
        return this;
    }

    public LeadStatus setPostbackStatus(Integer postbackStatus) {
        this.postbackStatus = postbackStatus;
        return this;
    }

    @Override
    public String toString() {
        return "LeadStatus{" +
                "leadStatus=" + leadStatus +
                ", partnerAssigningStatus=" + partnerAssigningStatus +
                ", partnerProcessingStatus=" + partnerProcessingStatus +
                ", partnerCheckStatus=" + partnerCheckStatus +
                ", voiceVerificationStatus=" + voiceVerificationStatus +
                ", postbackStatus=" + postbackStatus +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeadStatus actualLeadStatus = (LeadStatus) o;
        return Objects.equals(leadStatus, actualLeadStatus.leadStatus) &&
                Objects.equals(partnerAssigningStatus, actualLeadStatus.partnerAssigningStatus) &&
                Objects.equals(partnerProcessingStatus, actualLeadStatus.partnerProcessingStatus) &&
                Objects.equals(partnerCheckStatus, actualLeadStatus.partnerCheckStatus) &&
                Objects.equals(voiceVerificationStatus, actualLeadStatus.voiceVerificationStatus) &&
                Objects.equals(postbackStatus, actualLeadStatus.postbackStatus);

    }
}
