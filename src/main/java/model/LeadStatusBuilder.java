package model;

import javafx.util.Builder;

public class LeadStatusBuilder {
    private LeadStatus object;

    public LeadStatusBuilder create() {
        this.object = new LeadStatus();
        return this;
    }

    public LeadStatus build() {
        return object;
    }

    public LeadStatusBuilder setLeadStatus(Integer leadStatus) {
        object.setLeadStatus(leadStatus);
        return this;
    }
//
//    public LeadStatusBuilder getPartnerAssigningStatus() {
//        return object.getPartnerAssigningStatus();
//    }
//
//    public LeadStatusBuilder getPartnerProcessingStatus() {
//        return object.getPartnerProcessingStatus();
//    }
//
//    public LeadStatusBuilder getPartnerCheckStatus() {
//        return object.getPartnerCheckStatus();
//    }
//
//    public LeadStatusBuilder getVoiceVerificationStatus() {
//        return object.getVoiceVerificationStatus();
//    }
//
//    public LeadStatusBuilder getPostbackStatus() {
//        return object.getPostbackStatus();
//    }
//
//    public LeadStatusBuilder setLeadStatus(Integer leadStatus) {
//        return object.setLeadStatus(leadStatus);
//    }
//
//    public LeadStatusBuilder setPartnerAssigningStatus(Integer partnerAssigningStatus) {
//        return object.setPartnerAssigningStatus(partnerAssigningStatus);
//    }
//
//    public LeadStatusBuilder setPartnerProcessingStatus(Integer partnerProcessingStatus) {
//        return object.setPartnerProcessingStatus(partnerProcessingStatus);
//    }
//
//    public LeadStatusBuilder setPartnerCheckStatus(Integer partnerCheckStatus) {
//        return object.setPartnerCheckStatus(partnerCheckStatus);
//    }
//
//    public LeadStatusBuilder setVoiceVerificationStatus(Integer voiceVerificationStatus) {
//        return object.setVoiceVerificationStatus(voiceVerificationStatus);
//    }
//
//    public LeadStatusBuilder setPostbackStatus(Integer postbackStatus) {
//        return object.setPostbackStatus(postbackStatus);
//    }
}
