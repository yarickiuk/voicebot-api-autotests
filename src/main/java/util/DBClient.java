package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBClient {
    private static final String url = "jdbc:postgresql://10.10.40.10:6432/voicebot";
    private static final String user = "novikov";
    private static final String password = "Ub2Y7bBnFb2n57Te";
    private static Connection connection;

    private static DBClient instance;
    static {
        try {
            instance = new DBClient();
        } catch (SQLException e) {
            System.err.println("Подключение к базе не удалось");
            e.printStackTrace();
            System.exit(-1);
        }
    }
    private DBClient() throws SQLException {
        connection = DriverManager.getConnection(url, user, password);
    }

    public static DBClient getInstance() {
        return instance;
    }

    public String getUrlConnect() {
        return url;
    }

    public String getPassword() {
        return password;
    }

    public String getUser() {
        return user;
    }

    public Connection getConnection() {
        return connection;
    }
}
