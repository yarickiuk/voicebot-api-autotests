package util;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RMQFactoryUtil {
    private static final String url = "10.10.40.8";
    private static final String userName = "rmqadmin";
    private static final String password = "foh1poh9uu1E";
    private static final Integer port = 5672;

    private Connection connection;
    private Channel channel;

    public RMQFactoryUtil(String virtualHost) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(userName);
        factory.setPassword(password);
        factory.setHost(url);
        factory.setPort(port);
        factory.setVirtualHost(virtualHost);

        connection = factory.newConnection();
        channel = connection.createChannel();
    }

    public void publicMessage(String queueName, String message) throws IOException {
        AMQP.BasicProperties basicProperties = new AMQP.BasicProperties.Builder()
                .contentType("application/json")
                .deliveryMode(1)
                .build();
        channel.basicPublish("", queueName, basicProperties, message.getBytes());
        System.out.println(" [x] Sent '" + message + "'");
    }

    public void closeConnectionAndChannel() throws IOException, TimeoutException {
        channel.close();
        connection.close();
    }
}
